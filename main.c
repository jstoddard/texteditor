/* TextEditor
 * Copyright (C) 2016 Jeremiah Stoddard
 * A simple text editor to re-familiarize myself with GTK+
 *
 * TextEditor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdlib.h>
#include <gtk/gtk.h>

/* Create and populate the main window with the menubar and text entry
 * widgets
 */
void create_main_window (void);

/* Callbacks for the options in the menu bar */
void new_file (GtkMenuItem * menuitem, GtkTextBuffer * text_buffer);
void open_file (GtkMenuItem * menuitem, GtkTextBuffer * text_buffer);
void save_file (GtkMenuItem * menuitem, GtkTextBuffer * text_buffer);
void cut_command (GtkMenuItem * menuitem, GtkTextBuffer * textbuffer);
void copy_command (GtkMenuItem * menuitem, GtkTextBuffer * textbuffer);
void paste_command (GtkMenuItem * menuitem, GtkTextBuffer * textbuffer);

int
main (int argc, char *argv[])
{
  gtk_init (&argc, &argv);

  create_main_window ();

  gtk_main ();

  return 0;
}

/* Create and populate the main window with the menubar and text entry
 * widgets
 */
void
create_main_window (void)
{
  /* Containers */
  GtkWidget *window;
  GtkWidget *vbox;

  /* Menu */
  GtkWidget *menuitem_file_new;
  GtkWidget *menuitem_file_open;
  GtkWidget *menuitem_file_save;
  GtkWidget *menuitem_file_save_as;
  GtkWidget *menuitem_file_quit;
  GtkWidget *menuitem_edit_copy;
  GtkWidget *menuitem_edit_cut;
  GtkWidget *menuitem_edit_paste;
  GtkWidget *menu_file;
  GtkWidget *menu_edit;
  GtkWidget *menuitem_file;
  GtkWidget *menuitem_edit;
  GtkWidget *menubar;

  /* Text entry */
  GtkWidget *text_view;
  GtkTextBuffer *text_buffer;

  /* Create the window itself */
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (window), "Text Editor");
  gtk_window_set_default_size (GTK_WINDOW (window), 640, 480);
  g_signal_connect (window, "destroy", G_CALLBACK (gtk_main_quit), NULL);

  /* Create the file menu items */
  menuitem_file_new = gtk_menu_item_new_with_mnemonic ("_New");
  menuitem_file_open = gtk_menu_item_new_with_mnemonic ("_Open");
  menuitem_file_save = gtk_menu_item_new_with_mnemonic ("_Save");
  menuitem_file_save_as = gtk_menu_item_new_with_mnemonic ("Save _As");
  menuitem_file_quit = gtk_menu_item_new_with_mnemonic ("_Quit");
  /* Create the file menu itself and add the items to it */
  menu_file = gtk_menu_new ();
  gtk_menu_shell_append (GTK_MENU_SHELL (menu_file), menuitem_file_new);
  gtk_menu_shell_append (GTK_MENU_SHELL (menu_file), menuitem_file_open);
  gtk_menu_shell_append (GTK_MENU_SHELL (menu_file), menuitem_file_save);
  gtk_menu_shell_append (GTK_MENU_SHELL (menu_file), menuitem_file_save_as);
  gtk_menu_shell_append (GTK_MENU_SHELL (menu_file), menuitem_file_quit);
  /* Create a menu_item for the file menu so it can be appended to the
   * menubar. The file menu itself is "submenu" of the file menu_item.
   */
  menuitem_file = gtk_menu_item_new_with_mnemonic ("_File");
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem_file), menu_file);

  /* Create the edit menu items */
  menuitem_edit_copy = gtk_menu_item_new_with_mnemonic ("_Copy");
  menuitem_edit_cut = gtk_menu_item_new_with_label ("Cut");
  menuitem_edit_paste = gtk_menu_item_new_with_mnemonic ("_Paste");
  /* Create the edit menu itself and add the items to it */
  menu_edit = gtk_menu_new ();
  gtk_menu_shell_append (GTK_MENU_SHELL (menu_edit), menuitem_edit_copy);
  gtk_menu_shell_append (GTK_MENU_SHELL (menu_edit), menuitem_edit_cut);
  gtk_menu_shell_append (GTK_MENU_SHELL (menu_edit), menuitem_edit_paste);
  /* Create a menu_item for the edit menu and set the edit menu as a
   * submenu of the edit menu_item. */
  menuitem_edit = gtk_menu_item_new_with_mnemonic ("_Edit");
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem_edit), menu_edit);

  /* Create menu bar and append two menu_items: the file menu_item and
   * the edit menu_item
   */
  menubar = gtk_menu_bar_new ();
  gtk_menu_shell_append (GTK_MENU_SHELL (menubar), menuitem_file);
  gtk_menu_shell_append (GTK_MENU_SHELL (menubar), menuitem_edit);

  /* The text box */
  text_view = gtk_text_view_new ();
  text_buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (text_view));
  gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (text_view), GTK_WRAP_WORD_CHAR);

  /* Menu item signals */
  g_signal_connect (menuitem_file_new, "activate", G_CALLBACK (new_file),
		    text_buffer);
  g_signal_connect (menuitem_file_open, "activate", G_CALLBACK (open_file),
		    text_buffer);
  g_signal_connect (menuitem_file_save, "activate", G_CALLBACK (save_file),
		    text_buffer);
  g_signal_connect (menuitem_file_save_as, "activate", G_CALLBACK (save_file),
		    text_buffer);
  g_signal_connect (menuitem_file_quit, "activate",
		    G_CALLBACK (gtk_main_quit), NULL);
  g_signal_connect (menuitem_edit_copy, "activate", G_CALLBACK (copy_command),
		    text_buffer);
  g_signal_connect (menuitem_edit_cut, "activate", G_CALLBACK (cut_command),
		    text_buffer);
  g_signal_connect (menuitem_edit_paste, "activate",
		    G_CALLBACK (paste_command), text_buffer);

  /* Create a box to include the menu bar and the text view and add it to
   * the window
   */
  vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 1);
  gtk_box_pack_start (GTK_BOX (vbox), menubar, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox), text_view, TRUE, TRUE, 0);
  gtk_container_add (GTK_CONTAINER (window), vbox);

  /* Make things visible */
  gtk_widget_show_all (window);

  return;
}

/* User selected "New" from the file menu. */
void
new_file (GtkMenuItem * menuitem, GtkTextBuffer * text_buffer)
{
  /* The following line didn't work, for whatever reason... */
  /* gtk_text_buffer_set_text(text_buffer, "", -1); */
  /* ...so we get the start and end of the buffer, and delete it all. */
  GtkTextIter start, end;

  gtk_text_buffer_get_iter_at_offset (text_buffer, &start, 0);
  gtk_text_buffer_get_iter_at_offset (text_buffer, &end, -1);

  gtk_text_buffer_delete (text_buffer, &start, &end);
}

/* User selected "Open" from the file menu. */
void
open_file (GtkMenuItem * menuitem, GtkTextBuffer * text_buffer)
{
  GtkWidget *dialog;

  /* Show a file chooser dialog */
  dialog = gtk_file_chooser_dialog_new ("Open File", NULL,
					GTK_FILE_CHOOSER_ACTION_OPEN,
					"_Cancel", GTK_RESPONSE_CANCEL,
					"_Open", GTK_RESPONSE_ACCEPT, NULL);
  if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
    {
      gchar *filename;
      gchar *contents;
      gboolean result;
      /* g_file_get_contents and g_file_set_contents can crash your
       * program if error isn't NULL. It's probably a good practice (and
       * one I obviously didn't follow here) to set all pointers to NULL
       * when they're declared unless they are defined at that time as
       * well.
       */
      GError *error = NULL;

      filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));

      /* Open file */
      result = g_file_get_contents (filename, &contents, NULL, &error);
      if (result)
	{
	  gtk_text_buffer_set_text (text_buffer, contents, -1);
	  g_free (contents);
	}
      else
	{
	  /* An error occurred while reading the file. */
	  GtkWidget *errDialog;
	  errDialog = gtk_message_dialog_new (NULL, GTK_DIALOG_MODAL,
					      GTK_MESSAGE_ERROR,
					      GTK_BUTTONS_CLOSE,
					      "Could not open file: %s",
					      error->message);
	  gtk_dialog_run (GTK_DIALOG (errDialog));
	  gtk_widget_destroy (errDialog);
	  g_error_free (error);
	}

      g_free (filename);
    }
  gtk_widget_destroy (dialog);
}

/* User selected either "Save" or "Save As" from the file menu. Presently
 * the program doesn't distinguish between the two and just pops up a file
 * picker no matter what.
 */
void
save_file (GtkMenuItem * menuitem, GtkTextBuffer * text_buffer)
{
  GtkWidget *dialog;
  GtkTextIter start, end;
  gchar *text;

  /* Get text from text buffer */
  gtk_text_buffer_get_iter_at_offset (text_buffer, &start, 0);
  gtk_text_buffer_get_iter_at_offset (text_buffer, &end, -1);
  text = gtk_text_buffer_get_text (text_buffer, &start, &end, FALSE);

  /* Show a file chooser dialog */
  dialog = gtk_file_chooser_dialog_new ("Save File", NULL,
					GTK_FILE_CHOOSER_ACTION_SAVE,
					"_Cancel", GTK_RESPONSE_CANCEL,
					"_Save", GTK_RESPONSE_ACCEPT, NULL);
  /* If the user chooses an existing file, ask for confirmation */
  gtk_file_chooser_set_do_overwrite_confirmation (GTK_FILE_CHOOSER (dialog),
						  TRUE);

  if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
    {
      gchar *filename;
      gboolean result;
      /* error must be NULL going into g_file_set_contents. */
      GError *error = NULL;

      filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));

      /* Save the text to the selected filename */
      result = g_file_set_contents (filename, text, -1, &error);
      if (!result)
	{
	  /* An error occurred while writing the file. */
	  GtkWidget *errDialog;
	  errDialog = gtk_message_dialog_new (NULL, GTK_DIALOG_MODAL,
					      GTK_MESSAGE_ERROR,
					      GTK_BUTTONS_CLOSE,
					      "Could not save file: %s",
					      error->message);
	  gtk_dialog_run (GTK_DIALOG (errDialog));
	  gtk_widget_destroy (errDialog);
	  g_error_free (error);
	}

      g_free (filename);
    }

  gtk_widget_destroy (dialog);

  g_free (text);
}

/* User selected "Cut" from the edit menu. */
void
cut_command (GtkMenuItem * menuitem, GtkTextBuffer * textbuffer)
{
  gtk_text_buffer_cut_clipboard (textbuffer,
				 gtk_clipboard_get (GDK_SELECTION_CLIPBOARD),
				 TRUE);
}

/* User selected "Copy" from the edit menu. */
void
copy_command (GtkMenuItem * menuitem, GtkTextBuffer * textbuffer)
{
  gtk_text_buffer_copy_clipboard (textbuffer,
				  gtk_clipboard_get
				  (GDK_SELECTION_CLIPBOARD));
}

/* User selected "Paste" from the edit menu. */
void
paste_command (GtkMenuItem * menuitem, GtkTextBuffer * textbuffer)
{
  gtk_text_buffer_paste_clipboard (textbuffer,
				   gtk_clipboard_get
				   (GDK_SELECTION_CLIPBOARD), NULL, TRUE);
}
