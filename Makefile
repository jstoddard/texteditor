texteditor:	main.o
	gcc -o texteditor main.o `pkg-config gtk+-3.0 --libs`

main.o:	main.c
	gcc -c main.c `pkg-config gtk+-3.0 --cflags`

clean:
	rm texteditor main.o
